import java.time.LocalTime;

public class Microwave {

    public String model;
    public int volt;
    public double price;


    public void heatUp() {
        if (volt >= 120) {
            System.out.println("Heating up whatever you put in there! :)");
        } else {
            System.out.println("Uh oh, seems like there is not enough power for the microwave to properly work :(");
        }
    }

    public void printClock() {
        LocalTime time = LocalTime.now();
        System.out.println("Clock: " + time);
    }
    
}
