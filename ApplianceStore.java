import java.util.Scanner;    
public class ApplianceStore {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        Microwave[] microwaveSet = new Microwave[4];

        for (int i = 0; i < microwaveSet.length; i++) {

            microwaveSet[i] = new Microwave();
            System.out.println("Price: ");
            microwaveSet[i].price = keyboard.nextDouble();
            System.out.println("Voltage: ");
            microwaveSet[i].volt = keyboard.nextInt();
            System.out.println("Model: ");
            microwaveSet[i].model = keyboard.next();
            if (i == 0) {
                microwaveSet[0].printClock();
                microwaveSet[0].heatUp();
            }
            System.out.println("---------------------");
        }

        System.out.println("$"+microwaveSet[microwaveSet.length-1].price);
        System.out.println(microwaveSet[microwaveSet.length-1].volt + "V");
        System.out.println(microwaveSet[microwaveSet.length-1].model);

       keyboard.close();

    }


}
